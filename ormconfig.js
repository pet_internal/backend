import { config } from 'dotenv';

config();

export default {
    type: "mysql",
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    logging: false,
    entities: ["dist/services/schemas/**/*.js"],
    migrations: ["dist/database/migration/**/*.js"],
    cli: {
        entitiesDir: "src/services/schemas",
        migrationsDir: "src/database/migration"
    }
};