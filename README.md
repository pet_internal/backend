# development

$ yarn dev

Lưu ý: Trước khi push code nhứ chạy lệnh:

```yarn test```

# Migration

-   yarn run generate
-   yarn run migrate 
```
# Project Structure
├───src
│   ├───business
│   │   ├───logic
│   │   └───rule
│   ├───database
│   │   ├───migration
│   │   └───seed
│   ├───service
│   │   └───schemas
│   ├───use-case
│   │   ├───auth
│   │   ├───file
│   │   │   └───upload-file
│   │   ├───role
│   │   └───user
│   │       ├───change-password
│   │       ├───crud
│   │       └───get-current
│   └───util
└───uploads
```
