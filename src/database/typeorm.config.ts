import { ConnectionOptions } from 'typeorm';

import { GroupSchema } from '../services/schemas/group.schema';
import { PermSchema } from '../services/schemas/perm.schema';
import { UserSchema } from '../services/schemas/user.schema';
import { UserGroupSchema } from '../services/schemas/users-groups.schema';
import { config } from 'dotenv';

config();

const host = process.env.DB_HOST as string;
const port = +(process.env.DB_PORT as string);
const username = process.env.DB_USERNAME as string || 'root';
const password = process.env.DB_PASSWORD as string || 'root';
const database = process.env.DB_NAME as string;

const typeOrmConfig: ConnectionOptions = {
  type: 'mysql',
  host,
  port,
  username,
  password,
  database,
  logging: false,
  entities: [UserSchema, PermSchema, GroupSchema, UserGroupSchema],
  migrations: ['dist/database/migration/**/*.ts'],
};

export default typeOrmConfig;
