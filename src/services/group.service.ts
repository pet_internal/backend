import { getRepository } from 'typeorm';

import { GroupSchema } from './schemas/group.schema';

export interface IGroup {
  getAll(): Promise<GroupSchema[]>;
}
export class GroupService implements IGroup {
  async getAll(): Promise<any> {
    const groupRepo = getRepository(GroupSchema);
    const groups = await groupRepo.find();
    return { status: 'success', result: groups };
  }
}
